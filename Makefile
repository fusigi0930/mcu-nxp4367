export ROOT_DIR = $(shell pwd)
export BUILD_DIR = $(ROOT_DIR)/build

include makefile.conf

LDFLAGS += -nostdlib -Xlinker --gc-sections -Wl,-Map=$(BUILD_DIR)/$(MODULE_NAME).map -ggdb -lm -L$(BUILD_DIR)/lpc_chip_43xx -L$(BUILD_DIR)/lpc_board_nxp_43xx

SUBDIRS = lpc_chip_43xx/src \
	lpc_board_nxp_lpcxpresso_4337/src \
	rtos

#$(BUILD_DIR) $(BUILD_DIR)/$(MODULE_NAME).hex

.SUFFIXES =
.SUFFIXES = .c .o

OBJS = $(patsubst %.c,%.o,$(wildcard *.c))

ifeq ($(TYPE),4330)
	LD_FILE="$(ROOT_DIR)/linker_script_4330.ld"
else
	LD_FILE="$(ROOT_DIR)/linker_script.ld"
endif

all: all_sub_dirs secondary-outputs
	@echo compiler complete

all_sub_dirs:
	@echo build sub directories
	mkdir -p $(BUILD_DIR)
	@for sd in $(SUBDIRS); do make -C $$sd || exit1; done

# -llpc_board_nxp_43s67 -llpc_chip_43xx
$(MODULE_NAME).elf: linker_script.ld all_sub_dirs
	@echo "building target: $@"
	$(file >$(BUILD_DIR)/objects.rsp, $(shell find $(BUILD_DIR) -name "*.o"))
	$(CC) $(LDFLAGS) -T $(LD_FILE) -o $(BUILD_DIR)/$@ "@$(BUILD_DIR)/objects.rsp" -llpc_board_nxp_43s67 -llpc_chip_43xx
	@echo "build $@ completed"
	
$(MODULE_NAME).hex: $(MODULE_NAME).elf
	@echo "building target: $@"
	$(OBJCOPY) -O ihex $(BUILD_DIR)/$(MODULE_NAME).elf $(BUILD_DIR)/$@
	@echo "build $@ completed"

$(MODULE_NAME).bin: $(MODULE_NAME).elf
	@echo "building target: $@"
	$(OBJCOPY) -O binary $(BUILD_DIR)/$(MODULE_NAME).elf $(BUILD_DIR)/$@
	@echo "build $@ completed"
	
	
$(MODULE_NAME).lst: $(MODULE_NAME).elf
	@echo "building target: $@"
	$(OBJDUMP) -h -S $(BUILD_DIR)/$(MODULE_NAME).elf > $(BUILD_DIR)/$@
	@echo "build $@ completed"	

$(MODULE_NAME).siz: $(MODULE_NAME).elf
	@echo "building target: $@"
	$(SIZE) -x --format=sysv $(BUILD_DIR)/$(MODULE_NAME).elf
	$(SIZE) -x --format=berkeley $(BUILD_DIR)/$(MODULE_NAME).elf
	@echo "build $@ completed"	

secondary-outputs: $(MODULE_NAME).hex $(MODULE_NAME).bin $(MODULE_NAME).lst $(MODULE_NAME).siz
	mkdir -p $(BUILD_DIR)
	
clean:
	@for sd in $(dir $(SUBDIRS)); do make -C $$sd $@; done
	@rm -rf $(BUILD_DIR)
	
	
.PHONY: all clean dependence
.SECONDARY:
