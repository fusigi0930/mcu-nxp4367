/***********************************************************************//**
 * @file		lpc43xx_pinsel.c
 * @brief		Contains all functions support for Pin connect block firmware
 * 				library on LPC43xx
 * @version		1.0
 * @date		8. Sep. 2017
 * @author		ChuYuan
 **************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 **********************************************************************/

/* Peripheral group ----------------------------------------------------------- */
/** @addtogroup PINSEL
 * @{
 */

/* Includes ------------------------------------------------------------------- */
#include "lpc43xx_pinsel.h"

/* Public Functions ----------------------------------------------------------- */
/** @addtogroup PINSEL_Public_Functions
 * @{
 */
/*********************************************************************//**
 * @brief 		Configure trace function
 * @param[in] 	NewState State of the Trace function configuration,
 * 				should be one of the following:
 * 				- ENABLE : Enable Trace Function
 * 				- DISABLE : Disable Trace Function
 *
 * @return 		None
 **********************************************************************/
/*
void PINSEL_ConfigTraceFunc(FunctionalState NewState)
{
	if (NewState == ENABLE) {
		LPC_PINCON->PINSEL10 |= (0x01UL << 3);
	} else if (NewState == DISABLE) {
		LPC_PINCON->PINSEL10 &= ~(0x01UL << 3);
	}
}
*/
/*********************************************************************//**
 * @brief 		Setup I2C0 pins
 * @param[in]	i2cPinMode I2C pin mode,
 * 				should be one of the following:
 * 				- PINSEL_I2C_Normal_Mode : The standard drive mode
 * 				- PINSEL_I2C_Fast_Mode : Fast Mode Plus drive mode
 *
 * @param[in]	filterSlewRateEnable  should be:
 * 				- ENABLE: Enable filter and slew rate.
 * 				- DISABLE: Disable filter and slew rate.
 *
 * @return 		None
 **********************************************************************/
/*
void PINSEL_SetI2C0Pins(uint8_t i2cPinMode, FunctionalState filterSlewRateEnable)
{
	uint32_t regVal;

	if (i2cPinMode == PINSEL_I2C_Fast_Mode){
		regVal = PINSEL_I2CPADCFG_SCLDRV0 | PINSEL_I2CPADCFG_SDADRV0;
	}

	if (filterSlewRateEnable == DISABLE){
		regVal = PINSEL_I2CPADCFG_SCLI2C0 | PINSEL_I2CPADCFG_SDAI2C0;
	}
	LPC_PINCON->I2CPADCFG = regVal;
}
*/

/*********************************************************************//**
 * @brief 		Configure Pin corresponding to specified parameters passed
 * 				in the PinCfg
 * @param[in]	PinCfg	Pointer to a PINSEL_CFG_Type structure
 *                    that contains the configuration information for the
 *                    specified pin.
 * @return 		None
 **********************************************************************/

#define __IO volatile
#define __O volatile
#define __I volatile const
#define LPC_SCU_BASE              0x40086000


typedef struct {
	__IO uint32_t  SFSP[16][32];
	__I  uint32_t  RESERVED0[256];
	__IO uint32_t  SFSCLK[4];			/*!< Pin configuration register for pins CLK0-3 */
	__I  uint32_t  RESERVED16[28];
	__IO uint32_t  SFSUSB;				/*!< Pin configuration register for USB */
	__IO uint32_t  SFSI2C0;				/*!< Pin configuration register for I2C0-bus pins */
	__IO uint32_t  ENAIO[3];			/*!< Analog function select registerS */
	__I  uint32_t  RESERVED17[27];
	__IO uint32_t  EMCDELAYCLK;			/*!< EMC clock delay register */
	__I  uint32_t  RESERVED18[63];
	__IO uint32_t  PINTSEL[2];			/*!< Pin interrupt select register for pin int 0 to 3 index 0, 4 to 7 index 1. */
} LPC_SCU_TYPE;

static void cihpSCUPinMuxSet(uint8_t port, uint8_t pin, uint16_t modefunc)
{
	LPC_SCU_TYPE *pScu = (LPC_SCU_TYPE *) LPC_SCU_BASE;
	pScu->SFSP[port][pin] = modefunc;
}

void PINSEL_ConfigPin(PINSEL_CFG_Type *PinCfg)
{
	cihpSCUPinMuxSet(PinCfg->Portnum, PinCfg->Pinnum, PinCfg->Funcnum);
}


/**
 * @}
 */

/**
 * @}
 */

/* --------------------------------- End Of File ------------------------------ */
