/*
 * @brief SSP example
 * This example show how to use the SSP in 3 modes : Polling, Interrupt and DMA
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board.h"
#include "stdio.h"

#define DMSGOUT

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#if (defined(BOARD_KEIL_MCB_1857) || defined(BOARD_KEIL_MCB_4357) || \
	defined(BOARD_NGX_XPLORER_1830) || defined(BOARD_NGX_XPLORER_4330) || \
	defined(BOARD_NXP_LPCLINK2_4370) || defined(BOARD_NXP_LPCXPRESSO_4337) || \
	defined(BOARD_NXP_LPCXPRESSO_1837))
#define LPC_SSP           LPC_SSP0
#define SSP_IRQ           SSP0_IRQn
#define LPC_GPDMA_SSP_TX  GPDMA_CONN_SSP0_Tx
#define LPC_GPDMA_SSP_RX  GPDMA_CONN_SSP0_Rx
#define SSPIRQHANDLER SSP0_IRQHandler
#elif (defined(BOARD_HITEX_EVA_1850) || defined(BOARD_HITEX_EVA_4350))
#define LPC_SSP           LPC_SSP0
#define SSP_IRQ           SSP0_IRQn
#define LPC_GPDMA_SSP_TX  GPDMA_CONN_SSP0_Tx
#define LPC_GPDMA_SSP_RX  GPDMA_CONN_SSP0_Rx
#define SSPIRQHANDLER SSP0_IRQHandler
#elif
#else
#warning Unsupported Board
#endif
#define BUFFER_SIZE                         (64)
#define SSP_DATA_BITS                       (SSP_BITS_8)
#define SSP_DATA_BIT_NUM(databits)          (databits+1)
#define SSP_DATA_BYTES(databits)            (((databits) > SSP_BITS_8) ? 2:1)
#define SSP_LO_BYTE_MSK(databits)           ((SSP_DATA_BYTES(databits) > 1) ? 0xFF:(0xFF>>(8-SSP_DATA_BIT_NUM(databits))))
#define SSP_HI_BYTE_MSK(databits)           ((SSP_DATA_BYTES(databits) > 1) ? (0xFF>>(16-SSP_DATA_BIT_NUM(databits))):0)

/* Tx buffer */
static uint8_t Tx_Buf[BUFFER_SIZE];

/* Rx buffer */
static uint8_t Rx_Buf[BUFFER_SIZE];

static uint16_t g_nCount=0;

static SSP_ConfigFormat ssp_format;
static Chip_SSP_DATA_SETUP_T xf_setup;
static volatile uint8_t  isXferCompleted = 0;

static void procSpiReceive() {
	int key;

	xf_setup.tx_cnt = xf_setup.length;
	xf_setup.rx_cnt = 0;

	while (1) {
#if defined(DMSGOUT)
		for (uint8_t i=0; i < xf_setup.rx_cnt; i++) {
			if (0 == g_nCount % 32) {
				printf ("\r\n");
				g_nCount=0;
			}
			printf("%02x ", ((char*) xf_setup.rx_data)[i]);
			g_nCount++;
		}
#endif
		isXferCompleted=0;
		//Chip_SSP_ClearIntPending(LPC_SSP, SSP_INT_CLEAR_BITMASK);
		//Chip_SSP_Int_FlushData(LPC_SSP); // clean all rx fifo data
		if (sizeof(Rx_Buf) == xf_setup.rx_cnt) {
			for (uint32_t i = 0; i < xf_setup.length; i++) {
				Tx_Buf[i]=Rx_Buf[i];
			}
			xf_setup.rx_cnt = xf_setup.tx_cnt = 0;
		}

#if 0
		if (SSP_DATA_BYTES(ssp_format.bits) == 1) {
			Chip_SSP_Int_RWFrames8Bits(LPC_SSP, &xf_setup);
		}
		else {
			Chip_SSP_Int_RWFrames16Bits(LPC_SSP, &xf_setup);
		}
#endif
		//printf("tx: %d, rx: %d\r\n", xf_setup.tx_cnt, xf_setup.rx_cnt);

		Chip_SSP_Int_RxEnable(LPC_SSP);

		while (0 == isXferCompleted) {
#if 0
			key=DEBUGIN() & 0xff;
			if (0x0d == key || 0x20 == key) {
				printf("\r\n");
			}
#endif
			if (2 == isXferCompleted) {
				printf("receive error\r\n");
				return;
			}
		}

	}
}

static void process() {
	/* SSP initialization */
	Board_SSP_Init(LPC_SSP);

	Chip_SSP_Init(LPC_SSP);

	ssp_format.frameFormat = SSP_FRAMEFORMAT_SPI;
	ssp_format.bits = SSP_DATA_BITS;
	ssp_format.clockMode = SSP_CLOCK_MODE0;
	Chip_SSP_SetBitRate(LPC_SSP, 10000000);
    Chip_SSP_SetFormat(LPC_SSP, ssp_format.bits, ssp_format.frameFormat, ssp_format.clockMode);
	Chip_SSP_Enable(LPC_SSP);

	/* Setting SSP interrupt */
	NVIC_EnableIRQ(SSP_IRQ);

	Chip_SSP_SetMaster(LPC_SSP, 0);

	xf_setup.length = sizeof(Rx_Buf);
	xf_setup.tx_data = Tx_Buf;
	xf_setup.rx_data = Rx_Buf;

	procSpiReceive();

}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	SSP interrupt handler sub-routine
 * @return	Nothing
 */
void SSPIRQHANDLER(void)
{
	Chip_SSP_Int_Disable(LPC_SSP);	/* Disable all interrupt */
	//printf("%s\r\n", __FUNCTION__);
	if (SSP_DATA_BYTES(ssp_format.bits) == 1) {
		if (ERROR == Chip_SSP_Int_RWFrames8Bits(LPC_SSP, &xf_setup)) {
			isXferCompleted=2;
			return;
		}
	}
	else {
		if (ERROR == Chip_SSP_Int_RWFrames16Bits(LPC_SSP, &xf_setup)) {
			isXferCompleted=2;
			return;
		}
	}

#if 1
    if ((xf_setup.rx_cnt != xf_setup.length) || (xf_setup.tx_cnt != xf_setup.length)) {
    	Chip_SSP_Int_RxEnable(LPC_SSP);   /* enable all interrupts */
    }
    else {
    	isXferCompleted = 1;
    }
#else
    isXferCompleted = 1;
#endif
}

/**
 * @brief	Main routine for SSP example
 * @return	Nothing
 */
int main(void)
{
	SystemCoreClockUpdate();
	Board_Init();

	process();
	return 0;
}






