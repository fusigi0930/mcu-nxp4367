/*
 * FreeRTOS+IO V1.0.1 (C) 2012 Real Time Engineers ltd.
 *
 * FreeRTOS+IO is an add-on component to FreeRTOS.  It is not, in itself, part 
 * of the FreeRTOS kernel.  FreeRTOS+IO is licensed separately from FreeRTOS, 
 * and uses a different license to FreeRTOS.  FreeRTOS+IO uses a dual license
 * model, information on which is provided below:
 *
 * - Open source licensing -
 * FreeRTOS+IO is a free download and may be used, modified and distributed
 * without charge provided the user adheres to version two of the GNU General
 * Public license (GPL) and does not remove the copyright notice or this text.
 * The GPL V2 text is available on the gnu.org web site, and on the following
 * URL: http://www.FreeRTOS.org/gpl-2.0.txt
 *
 * - Commercial licensing -
 * Businesses and individuals who wish to incorporate FreeRTOS+IO into
 * proprietary software for redistribution in any form must first obtain a low
 * cost commercial license - and in-so-doing support the maintenance, support
 * and further development of the FreeRTOS+IO product.  Commercial licenses can
 * be obtained from http://shop.freertos.org and do not require any source files
 * to be changed.
 *
 * FreeRTOS+IO is distributed in the hope that it will be useful.  You cannot
 * use FreeRTOS+IO unless you agree that you use the software 'as is'.
 * FreeRTOS+IO is provided WITHOUT ANY WARRANTY; without even the implied
 * warranties of NON-INFRINGEMENT, MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. Real Time Engineers Ltd. disclaims all conditions and terms, be they
 * implied, expressed, or statutory.
 *
 * 1 tab == 4 spaces!
 *
 * http://www.FreeRTOS.org
 * http://www.FreeRTOS.org/FreeRTOS-Plus
 *
 */

#ifndef FREERTOS_IO_SSP_H
#define FREERTOS_IO_SSP_H

#include <chip_lpc43xx.h>

/*********************************************************************//**
 * SSP Interrupt Configuration defines
 **********************************************************************/
/** Receive Overrun */
#define SSP_INTCFG_ROR          SSP_RORIM
/** Receive TimeOut */
#define SSP_INTCFG_RT           SSP_RTIM
/** Rx FIFO is at least half full */
#define SSP_INTCFG_RX           SSP_RXIM
/** Tx FIFO is at least half empty */
#define SSP_INTCFG_TX           SSP_TXIM

/*********************************************************************//**
 * SSP Configured Interrupt Status defines
 **********************************************************************/
/** Receive Overrun */
#define SSP_INTSTAT_ROR         SSP_RORMIS
/** Receive TimeOut */
#define SSP_INTSTAT_RT          SSP_RTMIS
/** Rx FIFO is at least half full */
#define SSP_INTSTAT_RX          SSP_RXMIS
/** Tx FIFO is at least half empty */
#define SSP_INTSTAT_TX          SSP_TXMIS

/*********************************************************************//**
 * SSP Raw Interrupt Status defines
 **********************************************************************/
/** Receive Overrun */
#define SSP_INTSTAT_RAW_ROR             SSP_RORRIS
/** Receive TimeOut */
#define SSP_INTSTAT_RAW_RT              SSP_RTRIS
/** Rx FIFO is at least half full */
#define SSP_INTSTAT_RAW_RX              SSP_RXRIS
/** Tx FIFO is at least half empty */
#define SSP_INTSTAT_RAW_TX              SSP_TXRIS

/*********************************************************************//**
 * SSP Interrupt Clear defines
 **********************************************************************/
/** Writing a 1 to this bit clears the "frame was received when
 * RxFIFO was full" interrupt */
#define SSP_INTCLR_ROR          SSP_RORIC
/** Writing a 1 to this bit clears the "Rx FIFO was not empty and
 * has not been read for a timeout period" interrupt */
#define SSP_INTCLR_RT           SSP_RTIC

/*********************************************************************//**
 * Macro defines for SR register
 **********************************************************************/
/** SSP status TX FIFO Empty bit */
#define SSP_SR_TFE      SSP_STAT_TFE
/** SSP status TX FIFO not full bit */
#define SSP_SR_TNF      SSP_STAT_TNF
/** SSP status RX FIFO not empty bit */
#define SSP_SR_RNE      SSP_STAT_RNE
/** SSP status RX FIFO full bit */
#define SSP_SR_RFF      SSP_STAT_RFF
/** SSP status SSP Busy bit */
#define SSP_SR_BSY      SSP_STAT_BSY




/* These are not public functions.  Do not call these functions directly.  Call
 FreeRTOS_Open(), FreeRTOS_write(), FreeRTOS_read() and FreeRTOS_ioctl() only. */
portBASE_TYPE FreeRTOS_SSP_open(
		Peripheral_Control_t * const pxPeripheralControl);
size_t FreeRTOS_SSP_write(Peripheral_Descriptor_t const pxPeripheral,
		const void *pvBuffer, const size_t xBytes);
size_t FreeRTOS_SSP_read(Peripheral_Descriptor_t const pxPeripheralControl,
		void * const pvBuffer, const size_t xBytes);
portBASE_TYPE FreeRTOS_SSP_ioctl(
		Peripheral_Descriptor_t const pxPeripheralControl, uint32_t ulRequest,
		void *pvValue);

#endif /* FREERTOS_IO_SSP_H */

