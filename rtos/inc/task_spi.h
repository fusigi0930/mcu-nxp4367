#ifndef __TASK_SPI_H__
#define __TASK_SPI_H__

#define TASK_STACK_SIZE_SPI	256

#include <lwip/netif.h>
#include <semphr.h>

struct spi_ctrl {
	SemaphoreHandle_t semIRQ;
};

void taskSpiProc(void *pvParameters);

#if _SPI_NETPACK
err_t spi_enet_init(struct netif *netif);
#endif

#endif
