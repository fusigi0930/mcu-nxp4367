#include "snet.h"


#define SNET_PROC_CONTINUE		0x00
#define SNET_PROC_RETURN		0x01
#define SNET_PROC_RETRY			0x02
#define SNET_PROC_FINISH		0x03

#define SNET_PROC_MAX_RETRY		0x03

#define NET_PACKET_LENG			1536
#define SPI_FRAME_LENG			64

static uint32_t s_timer = 0;
static uint8_t s_procBlock = 0;
static int s_reqS1Leng = 0;

unsigned char proc_standby(void *data, add_action action);
unsigned char proc_ms_ready(void *data, add_action action);
unsigned char proc_res_waiting(void *data, add_action action);
unsigned char proc_sm_ready(void *data, add_action action);
unsigned char proc_pk_ready(void *data, add_action action);

unsigned char check_sum8(void* data, int leng) {
	unsigned char ret=0;
	unsigned char *ptr=data;
	int i=0;
	
	if (NULL == data || 0 == leng) return 0;
	
	while (i<leng) {
		ret ^= *ptr;
		ptr++;
		i++;
	}
	
	return ret;
}

unsigned short check_sum16(void* data, int leng) {
	unsigned short ret=0;
	unsigned short *ptr=data;
	int i=0;
	
	if (NULL == data || 0 == leng) return 0;
	
	if (1 == leng % 2) {
		while (i < leng-1) {
			ret ^= *ptr;
			ptr++;
			i+=2;
		}
		ret ^= *((unsigned char*) ptr);
	}
	else {
		while (i<leng) {
			ret ^= *ptr;
			ptr++;
			i+=2;
		}
	}
	
	return ret;
}

static void init_frame(struct SSpiNet *data, unsigned char cmd) {
	if (NULL == data) return;
	memset (data, 0, sizeof(struct SSpiNet));
	data->lead = _SNET_LEAD_BYTE;
	data->end = _SNET_END_BYTE;
	data->tag = cmd;
}

static void dummy_frame(struct SSpiNet *data) {
	if (NULL == data) return;
	
	memset (data, 0xcc, sizeof(struct SSpiNet));
}


static unsigned char g_current_status = _SNET_STATUS_S0;
static unsigned char g_next_status = _SNET_STATUS_S0;
static unsigned char *s_s1ReceData = NULL;
unsigned char *g_pResS3Data = NULL;
int g_resS3Leng = 0;

void snet_main_proc(void *data) {
	if (NULL == data) return;
	unsigned char procCode;
	
	switch (g_current_status) {
		default: break;
		
		case _SNET_STATUS_S0:
		{
			procCode = proc_standby(data, NULL);
		} break;
		case _SNET_STATUS_S1:
		{
			procCode = proc_ms_ready(data, NULL);
		} break;
		case _SNET_STATUS_S2:
		{
			procCode = proc_res_waiting(data, NULL);
		} break;
		case _SNET_STATUS_S3:
		{
			procCode = proc_sm_ready(data, NULL);
		} break;
		case _SNET_STATUS_PKS1:
		{
			procCode = proc_pk_ready(data, NULL);
		} break;
	}	
}

unsigned char proc_standby(void *data, add_action action) {
	struct SNetToSpi *netSpi = (struct SNetToSpi *) data; 
	struct SSpiNet *reqSpi=(struct SSpiNet *) g_arSpiReadBuf;
	struct SSpiNet *resSpi=(struct SSpiNet *) g_arSpiSendBuf;

	int nTotalRead=0, nRead=0;
	while (SPI_FRAME_LENG > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, g_arSpiReadBuf + nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead+=nRead;
	}
	
	unsigned char retCheck = check_spi_data(reqSpi);

	if (!(_SNET_CHECK_OK == retCheck || _SNET_CHECK_DUMMY == retCheck)) {
		// request data error
		init_frame(resSpi, _SNET_TAG_NET_MS);
		g_current_status = _SNET_STATUS_S0;
		g_next_status = _SNET_STATUS_S0;
		*resSpi->body = _SNET_RCODE_ERROR;
		resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
		FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));
		
		return SNET_PROC_RETURN;		
	}

	switch (reqSpi->tag) {
		default:
			init_frame(resSpi, _SNET_TAG_NET_MS);
			g_current_status = _SNET_STATUS_S0;
			g_next_status = _SNET_STATUS_S0;
			*resSpi->body = _SNET_RCODE_ERROR;
			resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
			break;
		case _SNET_TAG_NET_MS:
			s_reqS1Leng = *((int*) reqSpi->body);
			
			init_frame(resSpi, _SNET_TAG_NET_MS);
			g_current_status = _SNET_STATUS_S0;
			g_next_status = _SNET_STATUS_S1;
			*resSpi->body = _SNET_RCODE_OK;
			resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
			break;
		case _SNET_TAG_STORE_PK:
			s_reqS1Leng = *((int*) reqSpi->body);	
			
			init_frame(resSpi, _SNET_TAG_STORE_PK);
			g_current_status = _SNET_STATUS_S0;
			g_next_status = _SNET_STATUS_PKS1;
			*resSpi->body = _SNET_RCODE_OK;			
			resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
			break;
		case 0xcc:
			if (_SNET_CHECK_DUMMY == retCheck) {
				g_current_status = g_next_status;
				dummy_frame(resSpi);
			}
			break;
	}
	
	if (action) {
		action(data);
	}
		
	FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));
	
	return SNET_PROC_RETURN;
}

unsigned char proc_ms_ready(void *data, add_action action) {
	struct SNetToSpi *netSpi = (struct SNetToSpi *) data; 
	struct SSpiNet *reqSpi=(struct SSpiNet *) g_arSpiReadBuf;
	struct SSpiNet *resSpi=(struct SSpiNet *) g_arSpiSendBuf;
	
	if (NULL != s_s1ReceData) {
		free(s_s1ReceData);
		s_s1ReceData = NULL;
	}
	
	s_s1ReceData = (unsigned char*) malloc(NET_PACKET_LENG);
	memset (s_s1ReceData, 0, NET_PACKET_LENG);
	
	int nTotalRead=0, nRead=0;
	while (s_reqS1Leng > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, s_s1ReceData + nTotalRead, NET_PACKET_LENG - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		
		nTotalRead+=nRead;
		if (_SNET_CHECK_OK == check_spi_data((struct SSpiNet *) (s_s1ReceData + nTotalRead))) {
			struct SSpiNet *p = (struct SSpiNet *) (s_s1ReceData + nTotalRead);
			if (_SNET_TAG_NET_MS_FIN == p->tag) {
				// data not recevied completed
				init_frame(resSpi, _SNET_TAG_NET_MS_FIN);
				g_current_status = _SNET_STATUS_S0;
				g_next_status = _SNET_STATUS_S0;
				*resSpi->body = _SNET_RCODE_ERROR;
				resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
				FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
				return SNET_PROC_RETURN;
			}
		}
	}
	
	unsigned short uSum = check_sum16(s_s1ReceData, s_reqS1Leng);
	
	nTotalRead=0;
	while (SPI_FRAME_LENG > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, g_arSpiReadBuf + nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead += nRead;
	}
	
	if (_SNET_CHECK_OK != check_spi_data(reqSpi)) {
		init_frame(resSpi, _SNET_TAG_NET_MS_FIN);
		g_current_status = _SNET_STATUS_S0;
		g_next_status = _SNET_STATUS_S0;
		*resSpi->body = _SNET_RCODE_ERROR;
		resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
		FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
		return SNET_PROC_RETURN;	
	}
	
	init_frame(resSpi, _SNET_TAG_NET_MS_FIN);
	if (*((unsigned short *)reqSpi->body) == uSum) {
		*resSpi->body = _SNET_RCODE_OK;
		g_current_status = _SNET_STATUS_S2;
		g_resS3Leng = 0;
		if (g_pResS3Data) {
			free(g_pResS3Data);
			g_pResS3Data = NULL;
		}
	}
	else {
		*resSpi->body = _SNET_RCODE_ERROR;
		g_current_status = _SNET_STATUS_S0;
	}
	
	if (action) {
		action(data);
	}
		
	resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
	FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
	
	return SNET_PROC_RETURN;
}

unsigned char proc_res_waiting(void *data, add_action action) {
	struct SNetToSpi *netSpi = (struct SNetToSpi *) data; 
	struct SSpiNet *reqSpi=(struct SSpiNet *) g_arSpiReadBuf;
	struct SSpiNet *resSpi=(struct SSpiNet *) g_arSpiSendBuf;
	
	
	int nTotalRead=0, nRead=0;
	while (SPI_FRAME_LENG > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, g_arSpiReadBuf + nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead += nRead;
	}
	
	unsigned char checkRet = check_spi_data(reqSpi);
	
	if (_SNET_CHECK_DUMMY == checkRet) {
		return SNET_PROC_RETURN;
	}
		
	init_frame(resSpi, _SNET_TAG_NET_SM);
	*((unsigned short *) resSpi->body) = g_resS3Leng;
	resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
	
	if (0 != g_resS3Leng) {
		g_current_status = _SNET_STATUS_S3;
	}
	
	if (action) {
		action(data);
	}	
	FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
	
	return SNET_PROC_RETURN;
}

unsigned char proc_sm_ready(void *data, add_action action) {
	struct SNetToSpi *netSpi = (struct SNetToSpi *) data; 
	struct SSpiNet *reqSpi=(struct SSpiNet *) g_arSpiReadBuf;
	struct SSpiNet *resSpi=(struct SSpiNet *) g_arSpiSendBuf;
	int nLeng = g_resS3Leng;
	unsigned char *pData = g_pResS3Data;
	
	while (0 < nLeng) {
		FreeRTOS_read(netSpi->spi, g_arSpiReadBuf, sizeof(g_arSpiReadBuf));
		FreeRTOS_write(netSpi->spi, pData, sizeof(g_arSpiSendBuf));	
		nLeng -= sizeof(g_arSpiSendBuf);
		pData += sizeof(g_arSpiSendBuf);
	}
	
	unsigned short uSum = check_sum16(g_pResS3Data, g_resS3Leng);
	
	int nTotalRead=0, nRead=0;
	while (SPI_FRAME_LENG > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, g_arSpiReadBuf + nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead += nRead;
	}
	
	init_frame(resSpi, _SNET_TAG_NET_SM_FIN);
	if (_SNET_CHECK_OK != check_spi_data(reqSpi) || _SNET_TAG_NET_SM_FIN != reqSpi->tag) {
		*resSpi->body = _SNET_RCODE_ERROR;
	}	
	if (uSum != *((unsigned short *) reqSpi->body)) {
		*resSpi->body = _SNET_RCODE_ERROR;
	}
	
	*resSpi->body = _SNET_RCODE_OK;
	
	resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));

	if (action) {
		action(data);
	}
	
	if (g_pResS3Data) {
		free(g_pResS3Data);
		g_pResS3Data = NULL;
	}
	
	if (s_s1ReceData) {
		free(s_s1ReceData);
		s_s1ReceData = NULL;
	}
	
	g_current_status = _SNET_STATUS_S0;	
	
	FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
	
	return SNET_PROC_FINISH;
}

unsigned char proc_pk_ready(void *data, add_action action) {
	struct SNetToSpi *netSpi = (struct SNetToSpi *) data; 
	struct SSpiNet *reqSpi=(struct SSpiNet *) g_arSpiReadBuf;
	struct SSpiNet *resSpi=(struct SSpiNet *) g_arSpiSendBuf;
	
	if (NULL != s_s1ReceData) {
		free(s_s1ReceData);
		s_s1ReceData = NULL;
	}
	
	s_s1ReceData = (unsigned char*) malloc(NET_PACKET_LENG);
	memset (s_s1ReceData, 0, NET_PACKET_LENG);
	
	int nTotalRead=0, nRead=0;
	while (s_reqS1Leng > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, s_s1ReceData + nTotalRead, NET_PACKET_LENG - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		
		nTotalRead+=nRead;
		if (_SNET_CHECK_OK == check_spi_data((struct SSpiNet *) (s_s1ReceData + nTotalRead))) {
			struct SSpiNet *p = (struct SSpiNet *) (s_s1ReceData + nTotalRead);
			if (_SNET_TAG_STORE_PK_FIN == p->tag) {
				// data not recevied completed
				init_frame(resSpi, _SNET_TAG_STORE_PK_FIN);
				g_current_status = _SNET_STATUS_S0;
				g_next_status = _SNET_STATUS_S0;
				*resSpi->body = _SNET_RCODE_ERROR;
				resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
				FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
				return SNET_PROC_RETURN;
			}
		}
	}
	
	unsigned short uSum = check_sum16(s_s1ReceData, s_reqS1Leng);
	
	nTotalRead=0;
	while (SPI_FRAME_LENG > nTotalRead) {
		if (0 == (nRead = FreeRTOS_read(netSpi->spi, g_arSpiReadBuf + nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead += nRead;
	}
	
	if (_SNET_CHECK_OK != check_spi_data(reqSpi)) {
		init_frame(resSpi, _SNET_TAG_STORE_PK_FIN);
		g_current_status = _SNET_STATUS_S0;
		g_next_status = _SNET_STATUS_S0;
		*resSpi->body = _SNET_RCODE_ERROR;
		resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
		FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
		return SNET_PROC_RETURN;	
	}
	
	init_frame(resSpi, _SNET_TAG_STORE_PK_FIN);
	if (*((unsigned short *)reqSpi->body) == uSum) {
		*resSpi->body = _SNET_RCODE_OK;
		g_current_status = _SNET_STATUS_S0;
		g_resS3Leng = 0;
		if (g_pResS3Data) {
			free(g_pResS3Data);
			g_pResS3Data = NULL;
		}
	}
	else {
		*resSpi->body = _SNET_RCODE_ERROR;
		g_current_status = _SNET_STATUS_S0;
	}
	
	if (action) {
		action(data);
	}
		
	resSpi->sum = check_sum8(resSpi, _DATA_OFF(struct SSpiNet, sum));
	FreeRTOS_write(netSpi->spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));	
	
	return SNET_PROC_RETURN;
}

unsigned char check_spi_data(struct SSpiNet *data) {
	if (NULL == data) return _SNET_FATAL_ERROR;
	
	if (0xcccccccc == *((unsigned long *) data) && 0xcc == data->end)
		return _SNET_CHECK_DUMMY;
	
	if (_SNET_LEAD_BYTE != data->lead || _SNET_END_BYTE != data->end)
		return _SNET_FATAL_ERROR;
		
	switch (data->tag) {
		default: return _SNET_CHECK_INVALID_TAG;
		case _SNET_TAG_NET_MS:
		case _SNET_TAG_NET_MS_FIN:
		case _SNET_TAG_NET_SM:
		case _SNET_TAG_NET_SM_FIN:
		case _SNET_TAG_STORE_PK:
		case _SNET_TAG_STORE_PK_FIN:
		case _SNET_TAG_RESET_SLAVE:
			break;
	}
	
	if (data->sum != check_sum8(data, _DATA_OFF(struct SSpiNet, sum)))
		return _SNET_CHECK_SUM_ERROR;
		
	return _SNET_CHECK_OK;
}
