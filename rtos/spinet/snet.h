#ifndef __SPI_NET_PROTO_H__
#define __SPI_NET_PROTO_H__

#include "FreeRTOS.h"
#include "FreeRTOS_IO.h"

#define _SNET_TAG_NET_MS		0x00
#define _SNET_TAG_NET_MS_FIN	0x40
#define _SNET_TAG_NET_SM		0x80
#define _SNET_TAG_NET_SM_FIN	0xc0

#define _SNET_TAG_STORE_PK		0x01
#define _SNET_TAG_STORE_PK_FIN	0x41

#define _SNET_TAG_RESET_SLAVE	0xff

#define _SNET_STATUS_S0			0x00
#define _SNET_STATUS_S1			0x01
#define _SNET_STATUS_S2			0x02
#define _SNET_STATUS_S3			0x03
#define _SNET_STATUS_PKS1		0x04

#define _SNET_RCODE_OK			0x00
#define _SNET_RCODE_ERROR		0xff

#define _SNET_LEAD_BYTE			0x03
#define _SNET_END_BYTE			0x14

#define _SNET_CHECK_OK			0x00
#define _SNET_CHECK_DUMMY		0x01
#define _SNET_CHECK_INVALID_TAG	0x02
#define _SNET_CHECK_SUM_ERROR	0x03
#define _SNET_FATAL_ERROR		0xff

#define _DATA_OFF(e,exp) ((int) &(((e*) 0)->exp))

typedef unsigned char (*add_action)(void *);

extern unsigned char g_arSpiReadBuf[64];
extern unsigned char g_arSpiSendBuf[64];

extern unsigned char *g_pResS3Data;
extern int g_resS3Leng;

#pragma pack(push)
#pragma pack(1)
struct SSpiNet {
	unsigned char lead;
	unsigned char tag;
	unsigned char body[60];
	unsigned char sum;
	unsigned char end;
};
#pragma pack(pop)

struct SNetToSpi {
	Peripheral_Descriptor_t spi;
	unsigned char *data;
	unsigned char *res;
	int leng;
	int res_leng;
};

unsigned char check_sum8(void* data, int leng);
unsigned short check_sum16(void* data, int leng);

void snet_main_proc(void *data);

void udelay(uint32_t value);

unsigned char check_spi_data(struct SSpiNet *data);

#endif
