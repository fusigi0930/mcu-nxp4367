#include "FreeRTOS.h"
#include "task.h"

#include "task_spi.h"
#include "FreeRTOS_IO.h"
#include "queue.h"
#include "semphr.h"
#include <arch/sys_arch.h>

#include <board.h>
#include <snet.h>

uint8_t g_arSpiReadBuf[64];
uint8_t g_arSpiSendBuf[64];
uint8_t g_nCount=0;
extern uint8_t g_test;

#define BUFF_NUM_RX 8
#define BUFF_NUM_TX 8
#define tskRECPKT_PRIORITY (TCPIP_THREAD_PRIO - 1)

struct spi_enetdata {
	struct netif *netif;

	ENET_ENHTXDESC_T ptdesc[BUFF_NUM_TX];
	ENET_ENHRXDESC_T prdesc[BUFF_NUM_RX];

	struct pbuf *txpbufs[BUFF_NUM_TX];

	volatile u32_t tx_free_descs;
	u32_t tx_fill_idx;
	u32_t tx_reclaim_idx;

	struct pbuf *rxpbufs[BUFF_NUM_RX];
	volatile u32_t rx_free_descs;
	volatile u32_t rx_get_idx;
	u32_t rx_next_idx;

	sys_sem_t RxSem;
	sys_sem_t TxCleanSem;
	sys_mutex_t TxLockMutex;
	SemaphoreHandle_t xTXDCountSem;
} g_spi_enetdata;

struct spi_ctrl g_spi_ctrl;

#if (_SPI_NETPACK == 0)
void taskSpiProc(void *pvParameters) {
	Peripheral_Descriptor_t spi;

	(void) pvParameters;

	DEBUGOUT("[%s:%d] start task\r\n", __FUNCTION__, __LINE__);
#ifdef TYPE_4330
	spi = FreeRTOS_open("/SSP1/", 0);
#else
	spi = FreeRTOS_open("/SSP0/", 0);
#endif	
	configASSERT(spi);
#if 0
	FreeRTOS_ioctl(spi, ioctlSET_SPI_DATA_BITS, 		( void * ) SSP_BITS_8);
	FreeRTOS_ioctl(spi, ioctlSET_SPI_CLOCK_PHASE, 	( void * ) SSP_CPHA_FIRST );
	FreeRTOS_ioctl(spi, ioctlSET_SPI_CLOCK_POLARITY, 	( void * ) SSP_CPOL_HI );
	FreeRTOS_ioctl(spi, ioctlSET_SPI_MODE, 			( void * ) SSP_SLAVE_MODE );
	FreeRTOS_ioctl(spi, ioctlSET_SSP_FRAME_FORMAT, 	( void * ) SSP_FRAMEFORMAT_SPI );
	FreeRTOS_ioctl(spi, ioctlUSE_INTERRUPTS, (void *) pdTRUE);
#endif
	DEBUGOUT("[%s:%d] start task\r\n", __FUNCTION__, __LINE__);

	for (uint8_t i=0; i<sizeof(g_arSpiSendBuf); i++) {
		g_arSpiSendBuf[i]=(uint8_t) (0x50+i);
	}

	FreeRTOS_write(spi, g_arSpiSendBuf, sizeof(g_arSpiSendBuf));
	int nTotalRead=0;

	while (1) {
		int nRead=0;
#if 1
		if (0 == (nRead = FreeRTOS_read(spi, g_arSpiReadBuf+nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}

		nTotalRead += nRead;
		//DEBUGOUT("[%s:%d] read: %d\r\n", __FUNCTION__, __LINE__, nTotalRead);

		if (64 > nTotalRead) {
			//vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}

		for (uint8_t i=0; i < nTotalRead; i++) {
			if (0 == g_nCount % 32) {
				printf ("\r\n");
				g_nCount=0;
			}
			printf("%02x ", ((char*) g_arSpiReadBuf)[i]);
			g_nCount++;
		}

		nTotalRead=0;
#ifdef TYPE_4330
		Chip_SSP_ClearIntPending(LPC_SSP1, SSP_INT_CLEAR_BITMASK);
#else
		Chip_SSP_ClearIntPending(LPC_SSP0, SSP_INT_CLEAR_BITMASK);
#endif
		FreeRTOS_write(spi, g_arSpiReadBuf, sizeof(g_arSpiReadBuf));
		memset(g_arSpiReadBuf, 0, sizeof(g_arSpiReadBuf));
#endif
	}
}
#define spi_enet_init(a) while(0) { }
#else
/*
GET / HTTP/1.1
Host: www.hinet.net
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,* / *;q=0.8
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4
Cookie: _ga=GA1.2.573643544.1503557093
*/

uint8_t g_enetTest[] = {
	0x78, 0x48, 0x59, 0x64, 0x5b, 0x3f, 0x88, 0xd7, 0xf6, 0x52, 0x0d, 0xcf, 0x08, 0x00, 0x45, 0x00,
	0x01, 0xdf, 0x60, 0xc7, 0x40, 0x00, 0x40, 0x06, 0x41, 0x97, 0x0a, 0x0a, 0x01, 0x74, 0xd2, 0x3b,
	0xb9, 0x01, 0xa7, 0x5a, 0x00, 0x50, 0xb3, 0x48, 0x02, 0x51, 0xac, 0x0c, 0x05, 0x25, 0x80, 0x18,
	0x00, 0xe5, 0x98, 0x8c, 0x00, 0x00, 0x01, 0x01, 0x08, 0x0a, 0x04, 0xde, 0x62, 0x96, 0x9a, 0x25,
	0x81, 0xe7, 0x47, 0x45, 0x54, 0x20, 0x2f, 0x20, 0x48, 0x54, 0x54, 0x50, 0x2f, 0x31, 0x2e, 0x31,
	0x0d, 0x0a, 0x48, 0x6f, 0x73, 0x74, 0x3a, 0x20, 0x77, 0x77, 0x77, 0x2e, 0x68, 0x69, 0x6e, 0x65,
	0x74, 0x2e, 0x6e, 0x65, 0x74, 0x0d, 0x0a, 0x43, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x3a, 0x20, 0x6b, 0x65, 0x65, 0x70, 0x2d, 0x61, 0x6c, 0x69, 0x76, 0x65, 0x0d, 0x0a, 0x55,
	0x70, 0x67, 0x72, 0x61, 0x64, 0x65, 0x2d, 0x49, 0x6e, 0x73, 0x65, 0x63, 0x75, 0x72, 0x65, 0x2d,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x3a, 0x20, 0x31, 0x0d, 0x0a, 0x55, 0x73, 0x65,
	0x72, 0x2d, 0x41, 0x67, 0x65, 0x6e, 0x74, 0x3a, 0x20, 0x4d, 0x6f, 0x7a, 0x69, 0x6c, 0x6c, 0x61,
	0x2f, 0x35, 0x2e, 0x30, 0x20, 0x28, 0x58, 0x31, 0x31, 0x3b, 0x20, 0x4c, 0x69, 0x6e, 0x75, 0x78,
	0x20, 0x78, 0x38, 0x36, 0x5f, 0x36, 0x34, 0x29, 0x20, 0x41, 0x70, 0x70, 0x6c, 0x65, 0x57, 0x65,
	0x62, 0x4b, 0x69, 0x74, 0x2f, 0x35, 0x33, 0x37, 0x2e, 0x33, 0x36, 0x20, 0x28, 0x4b, 0x48, 0x54,
	0x4d, 0x4c, 0x2c, 0x20, 0x6c, 0x69, 0x6b, 0x65, 0x20, 0x47, 0x65, 0x63, 0x6b, 0x6f, 0x29, 0x20,
	0x43, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x2f, 0x35, 0x38, 0x2e, 0x30, 0x2e, 0x33, 0x30, 0x32, 0x39,
	0x2e, 0x39, 0x36, 0x20, 0x53, 0x61, 0x66, 0x61, 0x72, 0x69, 0x2f, 0x35, 0x33, 0x37, 0x2e, 0x33,
	0x36, 0x0d, 0x0a, 0x41, 0x63, 0x63, 0x65, 0x70, 0x74, 0x3a, 0x20, 0x74, 0x65, 0x78, 0x74, 0x2f,
	0x68, 0x74, 0x6d, 0x6c, 0x2c, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x2f, 0x78, 0x68, 0x74, 0x6d, 0x6c, 0x2b, 0x78, 0x6d, 0x6c, 0x2c, 0x61, 0x70, 0x70, 0x6c, 0x69,
	0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2f, 0x78, 0x6d, 0x6c, 0x3b, 0x71, 0x3d, 0x30, 0x2e, 0x39,
	0x2c, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x2f, 0x77, 0x65, 0x62, 0x70, 0x2c, 0x2a, 0x2f, 0x2a, 0x3b,
	0x71, 0x3d, 0x30, 0x2e, 0x38, 0x0d, 0x0a, 0x41, 0x63, 0x63, 0x65, 0x70, 0x74, 0x2d, 0x45, 0x6e,
	0x63, 0x6f, 0x64, 0x69, 0x6e, 0x67, 0x3a, 0x20, 0x67, 0x7a, 0x69, 0x70, 0x2c, 0x20, 0x64, 0x65,
	0x66, 0x6c, 0x61, 0x74, 0x65, 0x2c, 0x20, 0x73, 0x64, 0x63, 0x68, 0x0d, 0x0a, 0x41, 0x63, 0x63,
	0x65, 0x70, 0x74, 0x2d, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x3a, 0x20, 0x65, 0x6e,
	0x2d, 0x55, 0x53, 0x2c, 0x65, 0x6e, 0x3b, 0x71, 0x3d, 0x30, 0x2e, 0x38, 0x2c, 0x7a, 0x68, 0x2d,
	0x43, 0x4e, 0x3b, 0x71, 0x3d, 0x30, 0x2e, 0x36, 0x2c, 0x7a, 0x68, 0x3b, 0x71, 0x3d, 0x30, 0x2e,
	0x34, 0x0d, 0x0a, 0x43, 0x6f, 0x6f, 0x6b, 0x69, 0x65, 0x3a, 0x20, 0x5f, 0x67, 0x61, 0x3d, 0x47,
	0x41, 0x31, 0x2e, 0x32, 0x2e, 0x35, 0x37, 0x33, 0x36, 0x34, 0x33, 0x35, 0x34, 0x34, 0x2e, 0x31,
	0x35, 0x30, 0x33, 0x35, 0x35, 0x37, 0x30, 0x39, 0x33, 0x0d, 0x0a, 0x0d, 0x0a
};

#include <netif/etharp.h>

#define TRANSFER_STANDBY			0
#define TRANSFER_START_RECEIVE		1
#define TRANSFER_RECEIVE_COMPLETE	2
#define TRANSFER_START_SEND			3
#define TRANSFER_SEND_COMPLETE		4

#define PROC_STANDBY				0
#define PROC_RECEIVING				1
#define PROC_SEND					2

static uint8_t g_tranStatus=TRANSFER_STANDBY;
static uint8_t g_procStatus=PROC_STANDBY;

static uint8_t isDataComplete() {
	return TRANSFER_RECEIVE_COMPLETE;
}

static void spi_rxqueue_pbuf(struct spi_enetdata *spi_netdata, struct pbuf *p) {
	u32_t idx = spi_netdata->rx_next_idx;
	spi_netdata->rxpbufs[idx] = p;
	_DMSG("entry");

	spi_netdata->rx_free_descs--;
	idx++;
	if (BUFF_NUM_RX <= idx) {
		idx=0;
	}
	spi_netdata->rx_next_idx=idx;
}

s32_t spi_rx_queue(struct netif *netif) {
	struct spi_enetdata *spi_netdata = netif->state;
	struct pbuf *p=NULL;

	s32_t queued = 0;
	_DMSG("entry");

	while (0 < spi_netdata->rx_free_descs) {
		p = pbuf_alloc(PBUF_RAW, EMAC_ETH_MAX_FLEN, PBUF_RAM);
		if (NULL == p) {
			return queued;
		}

		spi_rxqueue_pbuf(spi_netdata, p);

		queued++;
	}
	return queued;
}

struct pbuf* spi_data2enet(struct netif *netif) {
	struct pbuf *p = NULL;
	struct spi_enetdata *spi_netdata = (struct spi_enetdata*) netif->state;
	u32_t status, ridx;
	int rxerr= 0;

	if (BUFF_NUM_RX == spi_netdata->rx_free_descs) {
		spi_rx_queue(netif);
		return NULL;
	}

	spi_netdata->rx_free_descs++;
	spi_rx_queue(netif);

	ridx = spi_netdata->rx_get_idx;
	p = spi_netdata->rxpbufs[ridx];

	if (NULL == p) {
		spi_rx_queue(netif);
		return NULL;
	}

	ridx++;
	if (BUFF_NUM_RX <= ridx) ridx=0;
	spi_netdata->rx_get_idx=ridx;
	// test code
	memcpy(p->payload, g_enetTest, sizeof(g_enetTest));
	return p;
}

void spi_enetif_input(struct netif *netif) {
	struct eth_hdr *ethhdr;
	struct pbuf *p;
	_DMSG("entry");
	p = spi_data2enet(netif);

	if (NULL == p) return;

	ethhdr = p->payload;

	switch (htons(ethhdr->type)) {
	case ETHTYPE_IP:
	case ETHTYPE_ARP:
#if PPPOE_SUPPORT
	case ETHTYPE_PPPOEDISC:
	case ETHTYPE_PPPOE:
#endif /* PPPOE_SUPPORT */
		/* full packet send to tcpip_thread to process */
		if (netif->input(p, netif) != ERR_OK) {
			LWIP_DEBUGF(NETIF_DEBUG,
						("lpc_enetif_input: IP input error\n"));
			/* Free buffer */
			pbuf_free(p);
		}
		break;

	default:
		/* Return buffer */
		pbuf_free(p);
		break;
	}
}

void taskSpiProc(void *pvParameters) {
	Peripheral_Descriptor_t spi;
	struct spi_enetdata *spi_netdata = (struct spi_enetdata*) pvParameters;
#ifdef TYPE_4330
	spi = FreeRTOS_open("/SSP1/", 0);
#else
	spi = FreeRTOS_open("/SSP0/", 0);
#endif
	configASSERT(spi);
	DEBUGOUT("starting %s task\r\n", __FUNCTION__);

	g_spi_ctrl.semIRQ = xSemaphoreCreateMutex();

	int nRead = 0;
	int nTotalRead = 0;

	while (1) {
		xSemaphoreTake(g_spi_ctrl.semIRQ, portMAX_DELAY);
#ifdef TYPE_4330
		Chip_SSP_ClearIntPending(LPC_SSP1, SSP_INT_CLEAR_BITMASK);
#else
		Chip_SSP_ClearIntPending(LPC_SSP0, SSP_INT_CLEAR_BITMASK);
#endif
		_DMSG("spi data incoming");
		if (0 == (nRead = FreeRTOS_read(spi, g_arSpiReadBuf+nTotalRead, sizeof(g_arSpiReadBuf) - nTotalRead))) {
			vTaskDelay(1 / portTICK_PERIOD_MS);
			continue;
		}
		nTotalRead += nRead;

		DEBUGOUT("read data length:%d\r\n", nTotalRead);

		if (TRANSFER_RECEIVE_COMPLETE != isDataComplete()) {
			continue;
		}

		spi_enetif_input(spi_netdata->netif);
		// just for testing
		FreeRTOS_write(spi, g_arSpiReadBuf, sizeof(g_arSpiReadBuf));
	}
}

extern void Board_ENET_GetMacADDR(u8_t *mac);

static err_t spi_net_tx_setup(struct spi_enetdata *spi_enetdata) {
	memset (&spi_enetdata->ptdesc[0], 0, sizeof(spi_enetdata->ptdesc));
	spi_enetdata->tx_free_descs = BUFF_NUM_TX;
	spi_enetdata->tx_fill_idx = 0;
	spi_enetdata-> tx_reclaim_idx = 0;
	_DMSG("entry");

	return ERR_OK;
}

static err_t spi_net_rx_setup(struct spi_enetdata *spi_enetdata) {
	memset (&spi_enetdata->prdesc[0], 0, sizeof(spi_enetdata->prdesc));
	_DMSG("entry");
	return ERR_OK;
}

static err_t spi_net_l1_init(struct netif *netif) {
	_DMSG("entry");
	if (ERR_OK != spi_net_tx_setup(&g_spi_enetdata)) {
		return ERR_BUF;
	}
	if (ERR_OK != spi_net_rx_setup(&g_spi_enetdata)) {
		return ERR_BUF;
	}
	return ERR_OK;
}

static err_t spi_etharp_output(struct netif *netif, struct pbuf *q, ip_addr_t ipaddr) {

	return ERR_CONN;
}

static err_t spi_low_level_output(struct netif *netif, struct pbuf *sendp) {
	return ERR_OK;
}

err_t spi_enet_init(struct netif *netif) {
	err_t err;
	if (NULL == netif) return ERR_IF;

	DEBUGOUT("[%s:%d] \r\n", __FUNCTION__, __LINE__);
	g_spi_enetdata.netif = netif;
	Board_ENET_GetMacADDR(netif->hwaddr);
	netif->hwaddr_len = 6;
	netif->mtu = 1500; // maximum transfer unit
	// device capability
	netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_UP |
					   NETIF_FLAG_ETHERNET;
	netif->state = &g_spi_enetdata;

	err = spi_net_l1_init(netif);
	if (err != ERR_OK) {
		return err;
	}

	netif->hostname = "spi-lwip";
	memcpy (netif->name, "en", 2);
	netif->output = spi_etharp_output;
	netif->linkoutput = spi_low_level_output;

	g_spi_enetdata.xTXDCountSem = xSemaphoreCreateCounting(BUFF_NUM_TX, BUFF_NUM_TX);
	err = sys_mutex_new(&g_spi_enetdata.TxLockMutex);
	err = sys_sem_new(&g_spi_enetdata.RxSem, 0);
	// create read thread
	DEBUGOUT("starting spi_receive_thread task\r\n");
	sys_thread_new("spi_receive_thread", taskSpiProc, netif->state,
						DEFAULT_THREAD_STACKSIZE, tskRECPKT_PRIORITY);
	err = sys_sem_new(&g_spi_enetdata.TxCleanSem, 0);
	// create send thread
	//sys_thread_new("spi_txclean_thread", vTransmitCleanupTask, netif->state, DEFAULT_THREAD_STACKSIZE, tskTXCLEAN_PRIORITY);

	return ERR_OK;
}
#endif
